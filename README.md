# Ubuntu Server

setup and install scripts

## new host

run `wget https://bitbucket.org/stangenberg/ubuntu-server/raw/master/init.sh -O - | bash`

* installs curl, git, zsh, oh-my-zsh and mc
* creates user thorben (with pub key, zsh and sudo) 
* configures ssh for PubkeyAuthentication only

### client setup

update your  `~/.ssh/config` file

```
Host hostname
    HostName 0.0.0.0                # the ip
    User thorben                    # the username
    IdentityFile ~/.ssh/id_rsa.pub  # local identitiy file 
    ForwardAgent yes                # enable agent forwarding
```

### security.sh

Apply basic security inspired by [The Fan Club]( see https://www.thefanclub.co.za/how-to/how-secure-ubuntu-1204-lts-server-part-1-basics)


#### Check for rootkits - RKHunter 

`sudo rkhunter --check --nocolors --skip-keypress`

#### Scan open ports - Nmap

`sudo nmap -v -sT -A localhost`

#### Analyse system LOG files - LogWatch

`sudo logwatch`

#### Apparmor

`sudo apparmor_status`

#### Audit your system security - Tiger

`sudo tiger -e`