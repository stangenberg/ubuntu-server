# install node.js
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs

# install c9 - https://github.com/c9/install
curl -L https://raw.githubusercontent.com/c9/install/master/install.sh | bash
