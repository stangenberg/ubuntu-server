# apply basic security
# see https://www.thefanclub.co.za/how-to/how-secure-ubuntu-1204-lts-server-part-1-basics

# Local Variables
ScriptName="Ubuntu Server Secure"
ScriptVersion="1.0.0"
UserName=$(whoami)
LogDay=$(date '+%Y-%m-%d')
LogTime=$(date '+%Y-%m-%d %H:%M:%S')
LogFile=/var/log/uss_$LogDay.log

ADMINISTRATOR_EMAIL="thorben@stangenberg.ch"
HOST_EMAIL="server-aro@stangenberg.ch"

# Firewall
echo "1. install & activate firewall"
	echo "$LogTime uss: [$UserName] 1. install & activate firewall" >> $LogFile 
	sudo apt-get install -y ufw
    sudo ufw reset
# defaults
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
# allow ssh, http and https
	sudo ufw allow 22/tcp
	sudo ufw allow 80/tcp
	sudo ufw allow 443/tcp
# Tutum ports
    sudo ufw allow 6783
    sudo ufw allow 48001/tcp
    sudo ufw allow 49153:65535/tcp
	sudo ufw enable

# shared memory
echo "2. secure shared memory"
	echo "$LogTime uss: [$UserName] 2. secure shared memory" >> $LogFile 
	fstab=$(grep -c "tmpfs" /etc/fstab)
	if [ ! "$fstab" -eq "0" ] 
	then
	  echo "# fstab already contains a tmpfs partition. Nothing to be done."
	  echo "$LogTime uss: [$UserName] fstab already contains a tmpfs partition. Nothing to be done." >> $LogFile
	fi
	if [ "$fstab" -eq "0" ]
	then
	  echo "# fstab being updated to secure shared memory"
	  echo "$LogTime uss: [$UserName] fstab being updated to secure shared memory" >> $LogFile
	  sudo echo "# $ScriptName Script Entry - Secure Shared Memory - $LogTime" >> /etc/fstab
	  sudo echo "tmpfs     /dev/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab
	  echo "# Shared memory secured. Reboot required"
	  echo "$LogTime uss: [$UserName] Shared memory secured. Reboot required" >> $LogFile
	fi

# network & sysctl 
echo "# 3. Harden network with sysctl settings"
	echo "$LogTime uss: [$UserName] 3. Harden network with sysctl settings" >> $LogFile 
	echo "# Updating sysctl network settings"
	echo "$LogTime uss: [$UserName] Updating sysctl network settings" >> $LogFile 
	# Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv4.conf.default.rp_filter entry exists comment out old entries" >> $LogFile 
	sysctlConfig1=$(grep -c "net.ipv4.conf.default.rp_filter" /etc/sysctl.conf)
	if [ ! "$sysctlConfig1" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.conf.default.rp_filter/#net.ipv4.conf.default.rp_filter/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv4.conf.all.rp_filter entry exists comment out old entries" >> $LogFile            
	sysctlConfig2=$(grep -c "net.ipv4.conf.all.rp_filter" /etc/sysctl.conf)
	if [ ! "$sysctlConfig2" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.conf.all.rp_filter/#net.ipv4.conf.all.rp_filter/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	  echo "$LogTime uss: [$UserName] Check if net.ipv4.icmp_echo_ignore_broadcasts entry exists comment out old entries" >> $LogFile            
	sysctlConfig3=$(grep -c "net.ipv4.icmp_echo_ignore_broadcasts" /etc/sysctl.conf)
	if [ ! "$sysctlConfig3" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.icmp_echo_ignore_broadcasts/#net.ipv4.icmp_echo_ignore_broadcasts/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv4.tcp_syncookies entry exists comment out old entries" >> $LogFile 
	sysctlConfig4=$(grep -c "net.ipv4.tcp_syncookies" /etc/sysctl.conf)
	if [ ! "$sysctlConfig4" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.tcp_syncookies/#net.ipv4.tcp_syncookies/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv4.conf.all.accept_source_route entry exists comment out old entries" >> $LogFile            
	sysctlConfig5=$(grep -c "net.ipv4.conf.all.accept_source_route" /etc/sysctl.conf)
	if [ ! "$sysctlConfig5" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.conf.all.accept_source_route/#net.ipv4.conf.all.accept_source_route/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	  echo "$LogTime uss: [$UserName] Check if net.ipv6.conf.all.accept_source_route entry exists comment out old entries" >> $LogFile            
	sysctlConfig6=$(grep -c "net.ipv6.conf.all.accept_source_route" /etc/sysctl.conf)
	if [ ! "$sysctlConfig6" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv6.conf.all.accept_source_route/#net.ipv6.conf.all.accept_source_route/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	          # Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv4.conf.default.accept_source_route entry exists comment out old entries" >> $LogFile 
	sysctlConfig7=$(grep -c "net.ipv4.conf.default.accept_source_route" /etc/sysctl.conf)
	if [ ! "$sysctlConfig7" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.conf.default.accept_source_route/#net.ipv4.conf.default.accept_source_route/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	echo "$LogTime uss: [$UserName] Check if net.ipv6.conf.default.accept_source_route entry exists comment out old entries" >> $LogFile            
	sysctlConfig8=$(grep -c "net.ipv6.conf.default.accept_source_route" /etc/sysctl.conf)
	if [ ! "$sysctlConfig8" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv6.conf.default.accept_source_route/#net.ipv6.conf.default.accept_source_route/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	# Check if sysctl entry exists comment out old entries
	  echo "$LogTime uss: [$UserName] Check if net.ipv4.conf.all.log_martians entry exists comment out old entries" >> $LogFile            
	sysctlConfig9=$(grep -c "net.ipv4.conf.all.log_martians" /etc/sysctl.conf)
	if [ ! "$sysctlConfig9" -eq "0" ] 
	 then
	    # if entry exists use sed to search and replace - write to tmp file - move to original
	    sudo sed 's/net.ipv4.conf.all.log_martians/#net.ipv4.conf.all.log_martians/g' /etc/sysctl.conf > /tmp/.sysctl_config
	    sudo mv /etc/sysctl.conf /etc/sysctl.conf.backup
	    sudo mv /tmp/.sysctl_config /etc/sysctl.conf
	fi
	echo "# Write new sysctl configuration settings"             
	echo "$LogTime uss: [$UserName] Write new sysctl configuration settings" >> $LogFile            
	sudo echo "# $ScriptName Script Entry - sysctl settings $LogTime" >> /etc/sysctl.conf
	sudo echo "net.ipv4.conf.default.rp_filter = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv4.conf.all.rp_filter = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
	sudo echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
	sudo echo "net.ipv6.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
	sudo echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	sudo echo "net.ipv6.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	sudo echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
	echo "# sysctl settings update complete"
		  echo "$LogTime uss: [$UserName] sysctl settings update complete" >> $LogFile            

	# reload sysctl
	sudo sysctl -p

# DenyHosts
echo "# 4. Scan logs and ban suspicious hosts - DenyHosts"
	echo "$LogTime uss: [$UserName] 4. Scan logs and ban suspicious hosts - DenyHosts" >> $LogFile
	echo "# Check if Denyhosts is installed..."
	echo "$LogTime uss: [$UserName] Check if Denyhosts is installed..." >> $LogFile
	if [ -f /usr/sbin/denyhosts ]
	  then
	     # RKHunter already installed
	     echo "# Denyhosts is already installed"
	     echo "$LogTime uss: [$UserName] Denyhosts is already installed" >> $LogFile
	fi
	if [ ! -f /usr/sbin/denyhosts ]
	  then
	     # Install DenyHosts
	     echo "# Install DenyHosts"
	     echo "$LogTime uss: [$UserName] Install DenyHosts" >> $LogFile
	     sudo apt-get install -y denyhosts 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Downloading at \2\/s, ETA \3/'
	   fi           
	# Make sure ADMIN_EMAIL entry does not exist
	echo "# Check if ADMIN_EMAIL option exists"
	echo "$LogTime uss: [$UserName] Check if ADMIN_EMAIL option exists" >> $LogFile           
	adminEmail=$(grep -c "ADMIN_EMAIL" /etc/denyhosts.conf )
	if [ ! "$adminEmail" -eq "0" ] 
	  then
	     # if entry exists use sed to search and replace - write to tmp file - move to original 
	     echo "# ADMIN_EMAIL entry exists. Commenting out old entries"
	     echo "$LogTime uss: [$UserName] ADMIN_EMAIL entry exists. Commenting out old entries" >> $LogFile            
	     sudo sed 's/ADMIN_EMAIL/#ADMIN_EMAIL/g' /etc/denyhosts.conf > /tmp/.denyhosts_config
	     sudo mv /etc/denyhosts.conf /etc/denyhosts.conf.backup
	     sudo mv /tmp/.denyhosts_config /etc/denyhosts.conf
	fi
	# Make sure order entry does not exist
	echo "# Check if SMTP_FROM entry exists"
	echo "$LogTime uss: [$UserName] Check if SMTP_FROM option exists" >> $LogFile           
	smtpFrom=$(grep -c "SMTP_FROM" /etc/denyhosts.conf )
	if [ ! "$smtpFrom" -eq "0" ] 
	  then
	     # if entry exists use sed to search and replace - write to tmp file - move to original 
	     echo "# SMTP_FROM entry exists. Commenting out old entries"
	     echo "$LogTime uss: [$UserName] SMTP_FROM entry exists. Commenting out old entries" >> $LogFile            
	     sudo sed 's/SMTP_FROM/#SMTP_FROM/g' /etc/denyhosts.conf > /tmp/.denyhosts_config
	     sudo mv /etc/denyhosts.conf /etc/denyhosts.conf.backup
	     sudo mv /tmp/.denyhosts_config /etc/denyhosts.conf
	fi
	# write new DenyHosts settings
	echo "# Write new DenyHosts configuration settings"             
	echo "$LogTime uss: [$UserName] Write new DenyHosts configuration settings" >> $LogFile         
	sudo echo "# $ScriptName Script Entry - DenyHosts settings $LogTime" >> /etc/denyhosts.conf
	sudo echo "ADMIN_EMAIL = $ADMINISTRATOR_EMAIL" >> /etc/denyhosts.conf
	sudo echo "SMTP_FROM = $HOST_EMAIL" >> /etc/denyhosts.conf            

	echo "# DenyHosts configuration settings update complete"
	echo "$LogTime uss: [$UserName] DenyHosts configuration settings update complete" >> $LogFile  
	echo "# Restart DenyHosts service"
	echo "$LogTime uss: [$UserName] Restart DenyHosts service" >> $LogFile          
	sudo /etc/init.d/denyhosts restart
	echo "# DenyHosts service restarted"
	echo "$LogTime uss: [$UserName] DenyHosts service restarted" >> $LogFile              
	 

# Intrusion Detection - PSAD	 
echo "# 5. Intrusion Detection - PSAD"
    echo "$LogTime uss: [$UserName] 5. Intrusion Detection - PSAD" >> $LogFile
    echo "# Check if PSAD is installed..."
    echo "$LogTime uss: [$UserName] Check if PSAD is installed..." >> $LogFile
    if [ -f /usr/sbin/psad ]
      then
         # PSAD already installed
         echo "# PSAD is already installed"
         echo "$LogTime uss: [$UserName] PSAD is already installed" >> $LogFile
    fi
    if [ ! -f /usr/sbin/psad ]
      then
         # Install PSAD
         echo "# Install PSAD"
         echo "$LogTime uss: [$UserName] Install PSAD" >> $LogFile
         sudo apt-get install -y psad
	   fi           
    # Enter Email address to receive notifications from PSAD
    psadEmail=$ADMINISTRATOR_EMAIL

    # Make sure EMAIL_ADDRESSES entry does not exist
    echo "# Check if EMAIL_ADDRESSES option exists"
    echo "$LogTime uss: [$UserName] Check if EMAIL_ADDRESSES option exists" >> $LogFile           
    psadAdminEmail=$(grep -c "EMAIL_ADDRESSES" /etc/psad/psad.conf )
    if [ ! "$psadAdminEmail" -eq "0" ] 
      then
         # if entry exists use sed to search and replace - write to tmp file - move to original 
         echo "# EMAIL_ADDRESSES entry exists. Commenting out old entries"
         echo "$LogTime uss: [$UserName] EMAIL_ADDRESSES entry exists. Commenting out old entries" >> $LogFile            
         sudo sed 's/EMAIL_ADDRESSES/#EMAIL_ADDRESSES/g' /etc/psad/psad.conf > /tmp/.psad_config
         sudo mv /etc/psad/psad.conf /etc/psad/psad.conf.backup
         sudo mv /tmp/.psad_config /etc/psad/psad.conf
    fi
    # Make sure ENABLE_AUTO_IDS entry does not exist
    echo "# Check if ENABLE_AUTO_IDS entry exists"
    echo "$LogTime uss: [$UserName] Check if ENABLE_AUTO_IDS option exists" >> $LogFile           
    psadIdsEmail=$(grep -c "ENABLE_AUTO_IDS_EMAILS" /etc/psad/psad.conf )
    if [ ! "$psadIdsEmail" -eq "0" ] 
      then
         # if entry exists use sed to search and replace - write to tmp file - move to original 
         echo "# ENABLE_AUTO_IDS entry exists. Commenting out old entries"
         echo "$LogTime uss: [$UserName] ENABLE_AUTO_IDS entry exists. Commenting out old entries" >> $LogFile            
         sudo sed 's/ENABLE_AUTO_IDS_EMAILS/#ENABLE_AUTO_IDS_EMAILS/g' /etc/psad/psad.conf > /tmp/.psad_config
         sudo mv /etc/psad/psad.conf /etc/psad/psad.conf.backup
         sudo mv /tmp/.psad_config /etc/psad/psad.conf
    fi
    # write new PSAD settings
    echo "# Write new PSAD configuration settings"             
    echo "$LogTime uss: [$UserName] Write new PSAD configuration settings" >> $LogFile         
    sudo echo "# $ScriptName Script Entry - DenyHosts settings $LogTime" >> /etc/psad/psad.conf
    sudo echo "EMAIL_ADDRESSES  $psadEmail;" >> /etc/psad/psad.conf
    sudo echo "ENABLE_AUTO_IDS_EMAILS Y;" >> /etc/psad/psad.conf
    echo "# PSAD configuration settings update complete"
    echo "$LogTime uss: [$UserName] PSAD configuration settings update complete" >> $LogFile  
    echo "# Update iptables to add log rules for PSAD"
	echo "$LogTime uss: [$UserName] Update iptables to add log rules for PSAD" >> $LogFile    
	sudo iptables -A INPUT -j LOG
    sudo iptables -A FORWARD -j LOG
    sudo ip6tables -A INPUT -j LOG
    sudo ip6tables -A FORWARD -j LOG    
	echo "# Update and Restart PSAD service"
	echo "$LogTime uss: [$UserName] Update and Restart PSAD service" >> $LogFile          
	sudo psad -R
    sudo psad --sig-update
    sudo psad -H
    echo "# PSAD service updated and restarted"
	echo "$LogTime uss: [$UserName] PSAD service updated restarted" >> $LogFile           

# Check for rootkits - RKHunter
echo "# 6. Check for rootkits - RKHunter"
	echo "$LogTime uss: [$UserName] 6. Check for rootkits - RKHunter" >> $LogFile
	echo "# Check if RKHunter is installed..."
	echo "$LogTime uss: [$UserName] Check if RKHunter is installed..." >> $LogFile
	if [ -f /usr/bin/rkhunter ]
	 then
	    # RKHunter already installed
	    echo "# RKHunter is already installed"
	    echo "$LogTime uss: [$UserName] RKHunter is already installed" >> $LogFile
	fi
	if [ ! -f /usr/bin/rkhunter ]
	 then
	    # Install RKHunter
	    echo "# RKHunter NOT installed, installing..."
	    echo "$LogTime uss: [$UserName] RKHunter NOT installed, installing..." >> $LogFile
	    sudo apt-get install -y rkhunter
	  fi
	  # Update RKHunter   
	echo "# Updating RKHunter"
	echo "$LogTime uss: [$UserName] Updating RKHunter" >> $LogFile                             
	sudo rkhunter --update
	sudo rkhunter --propupd
	echo "# RKHunter installed and updated"
	echo "$LogTime uss: [$UserName] RKHunter installed and updated" >> $LogFile   

# Scan open ports - Nmap
echo "$LogTime uss: [$UserName] 7. Scan open ports - Nmap" >> $LogFile
	echo "# 7. Scan open ports - Nmap"
	echo "# Check if Nmap is installed..."
	echo "$LogTime uss: [$UserName] Check if Nmap is installed..." >> $LogFile
	if [ -f /usr/bin/nmap ]
	then
	    # Nmap already installed
	    echo "# Nmap is already installed"
	    echo "$LogTime uss: [$UserName] Nmap is already installed" >> $LogFile
	fi
	if [ ! -f /usr/bin/nmap ]
	then
	   # Install Nmap
	    echo "# Nmap NOT installed, installing..."
	    echo "$LogTime uss: [$UserName] Nmap NOT installed, installing..." >> $LogFile
	    sudo apt-get install -y nmap
	fi
	echo "# Nmap installed"
	echo "$LogTime uss: [$UserName] Nmap installed" >> $LogFile   

# Analyse system LOG files - LogWatch
echo "$LogTime uss: [$UserName] 8. Analyse system LOG files - LogWatch" >> $LogFile
	echo "# 8. Analyse system LOG files - LogWatch"
	echo "# Check if LogWatch is installed..."
	echo "$LogTime uss: [$UserName] Check if LogWatch is installed..." >> $LogFile
	if [ -f /usr/sbin/logwatch ]
	then
	    # LogWatch already installed
	    echo "# LogWatch is already installed"
	    echo "$LogTime uss: [$UserName] LogWatch is already installed" >> $LogFile
	fi
	if [ ! -f /usr/sbin/logwatch ]
	then
	    # Install LogWatch
	    echo "# LogWatch NOT installed, installing..."
	    echo "$LogTime uss: [$UserName] LogWatch NOT installed, installing..." >> $LogFile
	    sudo apt-get install -y logwatch libdate-manip-perl
	fi
	echo "# LogWatch installed"
	echo "$LogTime uss: [$UserName] LogWatch installed" >> $LogFile   

# SELinux - Apparmor
echo "$LogTime uss: [$UserName] 9. SELinux - Apparmor" >> $LogFile
	echo "# 9. SELinux - Apparmor"
	echo "# Check if Apparmor is installed..."
	echo "$LogTime uss: [$UserName] Check if Apparmor is installed..." >> $LogFile
	if [ -f /usr/sbin/apparmor_status ]
	then
	    # Apparmor already installed
	    echo "# Apparmor is already installed"
	    echo "$LogTime uss: [$UserName] Apparmor is already installed" >> $LogFile
	fi
	if [ ! -f /usr/sbin/apparmor_status ]
	then
	   # Install Apparmor
	    echo "# Apparmor NOT installed, installing..."
	    echo "$LogTime uss: [$UserName] Apparmor NOT installed, installing..." >> $LogFile
	    sudo apt-get install -y apparmor apparmor-profiles
	  fi
	echo "# Apparmor installed"
	echo "$LogTime uss: [$UserName] Apparmor installed" >> $LogFile   

# Audit your system security - Tiger
echo "$LogTime uss: [$UserName] 10. Audit your system security - Tiger" >> $LogFile
	echo "# 17. Audit your system security - Tiger"
	echo "# Check if Tiger is installed..."
	echo "$LogTime uss: [$UserName] Check if Tiger is installed..." >> $LogFile
	if [ -f /usr/sbin/tiger ]
	then
	    # Tiger already installed
	    echo "# Tiger is already installed"
	    echo "$LogTime uss: [$UserName] Tiger is already installed" >> $LogFile
	fi
	if [ ! -f /usr/sbin/tiger ]
	then
	   # Install Tiger
	    echo "# Tiger NOT installed, installing..."
	    echo "$LogTime uss: [$UserName] Tiger NOT installed, installing..." >> $LogFile
	    sudo apt-get install -y tiger
	fi
	echo "# Tiger installed"
	echo "$LogTime uss: [$UserName] Tiger installed" >> $LogFile   
	
