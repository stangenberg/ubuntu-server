# Local Variables
ScriptName="Ubuntu Server"
ScriptVersion="1.0.0"
UserName=$(whoami)
LogDay=$(date '+%Y-%m-%d')
LogTime=$(date '+%Y-%m-%d %H:%M:%S')
LogFile=/var/log/uss_$LogDay.log

log() {
	echo "$LogTime uss: [$UserName] $1" >> $LogFile
	echo $1
}

# update locales 
locale-gen en_US en_US.UTF-8 de_CH de_CH.UTF-8
sleep 1

# use goolge dns
log "configure system to use google dns"
sed 's/nameserver/#nameserver/g' /etc/resolv.conf > /tmp/.resolv.conf
mv /etc/resolv.conf /etc/resolv.conf.backup
mv /tmp/.resolv.conf /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf
echo "nameserver 2001:4860:4860::8888" >> /etc/resolv.conf
echo "nameserver 2001:4860:4860::8844" >> /etc/resolv.conf

log "updateing apt"
apt-get update

log "installing packages"
apt-get install -y \
    curl \
    git \
    git-extras \
    mc \
    zsh \
    zsh-doc

sleep 3

# add user thorben
log "adding user thorben" 

# userdel -r thorben &>/dev/null
# sleep 1

id -u thorben &>/dev/null || adduser --disabled-password --gecos "" thorben
sleep 1

# disable sudo password for thorben 
sudoThorben=$(grep -c "thorben" /etc/sudoers)
if [ ! "$sudoThorben" -eq "0" ] 
then
	# if entry exists use sed to search and replace - write to tmp file - move to original
    sed 's/thorben/#thorben/g' /etc/sudoers > /tmp/.sudoers
    mv /etc/sudoers /etc/sudoers.backup
    mv /tmp/.sudoers /etc/sudoers
fi
echo "thorben ALL=NOPASSWD: ALL" >> /etc/sudoers
sleep 1

# copy public shh key
su -c "mkdir -p ~/.ssh" thorben
sleep 1
su -c "wget https://bitbucket.org/thorben/public-ssh/raw/master/thorben@stangenberg.net.pub -O ~/.ssh/authorized_keys" thorben
sleep 1
su -c "chmod 700 ~/.ssh" thorben
su -c "chmod 600 ~/.ssh/authorized_keys" thorben

# install oh-my-zsh for thorben
log "install oh-my-zsh"
su -c "cd /home/thorben && wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh" thorben
sleep 1
usermod -s /usr/bin/zsh thorben
sleep 1

# secure ssh 
# setup ssh for key only / no root
log "secure ssh config"
sshNewPort=22

sed 's/Port/#Port/g' /etc/ssh/sshd_config > /tmp/.sshd_config
mv /etc/ssh/sshd_config /etc/ssh/ssh_config.backup
mv /tmp/.sshd_config /etc/ssh/sshd_config

sshconfigProtocol=$(grep -c "Protocol" /etc/ssh/sshd_config)
if [ ! "$sshconfigProtocol" -eq "0" ] 
 then
    # if entry exists use sed to search and replace - write to tmp file - move to original
    sed 's/Protocol/#Protocol/g' /etc/ssh/sshd_config > /tmp/.sshd_config
    mv /etc/ssh/sshd_config /etc/ssh/ssh_config.backup
    mv /tmp/.sshd_config /etc/ssh/sshd_config
fi

sshconfigPermitRoot=$(grep -c "PermitRootLogin" /etc/ssh/sshd_config)
if [ ! "$sshconfigPermitRoot" -eq "0" ] 
 then
    # if entry exists use sed to search and replace - write to tmp file - move to original 
    sed 's/PermitRootLogin/#PermitRootLogin/g' /etc/ssh/sshd_config > /tmp/.sshd_config
    mv /etc/ssh/sshd_config /etc/ssh/ssh_config.backup
    mv /tmp/.sshd_config /etc/ssh/sshd_config
fi

sshconfigPermitPassword=$(grep -c "PasswordAuthentication" /etc/ssh/sshd_config)
if [ ! "$sshconfigPermitPassword" -eq "0" ] 
 then
    # if entry exists use sed to search and replace - write to tmp file - move to original 
    sed 's/PasswordAuthentication/#PasswordAuthentication/g' /etc/ssh/sshd_config > /tmp/.sshd_config
    mv /etc/ssh/sshd_config /etc/ssh/ssh_config.backup
    mv /tmp/.sshd_config /etc/ssh/sshd_config
fi
sshconfigAcceptEnv=$(grep -c "AcceptEnv" /etc/ssh/sshd_config)
if [ ! "$sshconfigAcceptEnv" -eq "0" ]
 then
    # if entry exists use sed to search and replace - write to tmp file - move to original
    sed 's/AcceptEnv/#AcceptEnv/g' /etc/ssh/sshd_config > /tmp/.sshd_config
    mv /etc/ssh/sshd_config /etc/ssh/ssh_config.backup
    mv /tmp/.sshd_config /etc/ssh/sshd_config
fi

echo "Port $sshNewPort" >> /etc/ssh/sshd_config
echo "Protocol 2" >> /etc/ssh/sshd_config
echo "PermitRootLogin no" >> /etc/ssh/sshd_config
echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
echo "PermitUserEnvironment yes" >> /etc/ssh/sshd_config
echo "AcceptEnv LANG LC_* GIT_* TUTUM_*" >> /etc/ssh/sshd_config

log "restart ssh deamon"
service ssh restart

log "initialization complete"
# exit
